const COUNTRIES = {
  USA: {
    label: 'США',
    emoji: '🇺🇸',
    wiki: 'https://en.wikipedia.org/wiki/United_States'
  },
  Canada: {
    label: 'Канада',
    emoji: '🇨🇦',
    wiki: 'https://en.wikipedia.org/wiki/Canada'
  },
  Kyrgyzstan: {
    label: 'Киргизстан',
    emoji: '🇰🇬',
    wiki: 'https://en.wikipedia.org/wiki/Kyrgyzstan'
  },
  Kazakhstan: {
    label: 'Казахстан',
    emoji: '🇰🇿',
    wiki: 'https://en.wikipedia.org/wiki/Kazakhstan'
  },
  Russia: {
    label: 'Россия',
    emoji: '🇷🇺',
    wiki: 'https://en.wikipedia.org/wiki/Russia'
  },
  China: {
    label: 'Китай',
    emoji: '🇨🇳',
    wiki: 'https://en.wikipedia.org/wiki/China'
  },
  Brazil: {
    label: 'Бразилия',
    emoji: '🇧🇷',
    wiki: 'https://en.wikipedia.org/wiki/Brazil'
  },
  India: {
    label: 'Индия',
    emoji: '🇮🇳',
    wiki: 'https://en.wikipedia.org/wiki/India'
  },
  Mexico: {
    label: 'Мексика',
    emoji: '🇲🇽',
    wiki: 'https://en.wikipedia.org/wiki/Mexico'
  },
  France: {
    label: 'Франция',
    emoji: '🇫🇷',
    wiki: 'https://en.wikipedia.org/wiki/France'
  },
  Germany: {
    label: 'Германия',
    emoji: '🇩🇪',
    wiki: 'https://en.wikipedia.org/wiki/Germany'
  },
  UK: {
    label: 'Великобритания',
    emoji: '🇬🇧',
    wiki: 'https://en.wikipedia.org/wiki/United_Kingdom'
  },
  Japan: {
    label: 'Япония',
    emoji: '🇯🇵',
    wiki: 'https://en.wikipedia.org/wiki/Japan'
  },
  Italy: {
    label: 'Италия',
    emoji: '🇮🇹',
    wiki: 'https://en.wikipedia.org/wiki/Italy'
  },
  Australia: {
    label: 'Австралия',
    emoji: '🇦🇺',
    wiki: 'https://en.wikipedia.org/wiki/Australia'
  },
  Spain: {
    label: 'Испания',
    emoji: '🇪🇸',
    wiki: 'https://en.wikipedia.org/wiki/Spain'
  },
  Southkorea: {
    label: 'Южная Корея',
    emoji: '🇰🇷',
    wiki: 'https://en.wikipedia.org/wiki/South_Korea'
  },
  Netherlands: {
    label: 'Нидерланды',
    emoji: '🇳🇱',
    wiki: 'https://en.wikipedia.org/wiki/Netherlands'
  },
  Turkey: {
    label: 'Турция',
    emoji: '🇹🇷',
    wiki: 'https://en.wikipedia.org/wiki/Turkey'
  },
  Saudiarabia: {
    label: 'Саудовская Аравия',
    emoji: '🇸🇦',
    wiki: 'https://en.wikipedia.org/wiki/Saudi_Arabia'
  },
  Argentina: {
    label: 'Аргентина',
    emoji: '🇦🇷',
    wiki: 'https://en.wikipedia.org/wiki/Argentina'
  },
  Switzerland: {
    label: 'Швейцария',
    emoji: '🇨🇭',
    wiki: 'https://en.wikipedia.org/wiki/Switzerland'
  },
}

const COUNTRIES_RU = {
  США: COUNTRIES.USA,
  Канада: COUNTRIES.Canada,
  Киргизия: COUNTRIES.Kyrgyzstan,
  Казахстан: COUNTRIES.Kazakhstan,
  Россия: COUNTRIES.Russia,
  Китай: COUNTRIES.China,
  Бразилия: COUNTRIES.Brazil,
  Индия: COUNTRIES.India,
  Мексика: COUNTRIES.Mexico,
  Франция: COUNTRIES.France,
  Германия: COUNTRIES.Germany,
  Великобритания: COUNTRIES.UK,
  Япония: COUNTRIES.Japan,
  Италия: COUNTRIES.Italy,
  Австралия: COUNTRIES.Australia,
  Испания: COUNTRIES.Spain,
  ['Южная Корея']: COUNTRIES.Southkorea,
  Нидерланды: COUNTRIES.Netherlands,
  Турция: COUNTRIES.Turkey,
  ['Саудовская Аравия']: COUNTRIES.Saudiarabia,
  Аргентина: COUNTRIES.Argentina,
  Швейцария: COUNTRIES.Switzerland,
}

module.exports = { COUNTRIES, COUNTRIES_RU }
