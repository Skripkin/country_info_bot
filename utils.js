const { COUNTRIES, COUNTRIES_RU } = require('./constants.js')
const Tie = require('./Tie.js')

function createCountryTie(countries) {
  const trie = new Tie()

  for (const country in countries) {
    trie.insert(country.toLowerCase())
  }

  return trie
}

const countryTie = createCountryTie(COUNTRIES)
const countryRuTie = createCountryTie(COUNTRIES_RU)

const getCountriesByList = function(message, countries, tie) {
  const lowerMessage = message.toLowerCase()
  const foundCountries = tie.search(lowerMessage)

  return foundCountries.map(n => {
    const keys = Object.keys(countries)
    const name = keys.find(k => k.toLowerCase() === n)
    return countries[name]
  })
}

const getCountry = function(message) {
  let countries = getCountriesByList(message, COUNTRIES, countryTie)

  if (!countries.length) {
    countries = getCountriesByList(message, COUNTRIES_RU, countryRuTie)
  }

  return countries
}

module.exports = {
  getCountry
}
