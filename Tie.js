class TieNode {
  constructor() {
    this.children = new Map()
    this.isEndOfWord = false
  }
}

class Tie {
  constructor() {
    this.root = new TieNode()
  }

  insert(word) {
    let node = this.root
    for (const char of word) {
      if (!node.children.has(char)) {
        node.children.set(char, new TieNode())
      }
      node = node.children.get(char)
    }
    node.isEndOfWord = true
  }

  search(prefix) {
    let node = this.root
    for (const char of prefix) {
      if (!node.children.has(char)) {
        return []
      }
      node = node.children.get(char)
    }
    return this._findAllWords(node, prefix)
  }

  _findAllWords(node, prefix) {
    const words = []
    if (node.isEndOfWord) {
      words.push(prefix)
    }
    for (const [char, childNode] of node.children) {
      words.push(...this._findAllWords(childNode, prefix + char))
    }
    return words
  }
}

module.exports = Tie
