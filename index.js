const TgApi = require('node-telegram-bot-api')
const { COUNTRIES, COUNTRIES_RU } = require('./constants')
const { getCountry } = require('./utils')

const BOT_TOKEN = '6434518911:AAEoZ88qTiip6AmN1f_Eb3sUGeIl7OLTAd8'

const bot = new TgApi(BOT_TOKEN, { polling: true })

bot.setMyCommands([
  { command: '/start', description: 'Начало работы' },
  { command: '/list', description: 'Список стран' }
])

bot.on('message', async msg => {
  const text = msg.text
  const chatId = msg.chat.id

  if (text === '/start') {
    const firstName = msg.chat.first_name
    await bot.sendMessage(
      chatId,
      `Добро пожаловать${firstName ? `, ${firstName}` : ''}! Я бот, отправляющий эмодзи и ссылку на википедию страны, которую ты введешь.\nВведи название страны или начальные буквы, чтобы продолжить.`
    )
    return
  }

  if (text === '/list') {
    await bot.sendMessage(
      chatId,
      `RU:\n${Object.keys(COUNTRIES_RU).join('\n')}\n---------\nEN:\n${Object.keys(COUNTRIES).join('\n')}` 
    )
    return
  }

  const lowerText = text.toLowerCase()

  let message = `Не найдено стран по запросу: ${text}`
  const foundCountries = getCountry(lowerText)

  if (foundCountries.filter(Boolean).length)
    message = foundCountries.map(({ label, emoji, wiki }) => `Страна: ${label}\nФлаг: ${emoji}\nПодробнее: ${wiki}`).join('\n\n')

  await bot.sendMessage(
    chatId,
    message
  )
})
